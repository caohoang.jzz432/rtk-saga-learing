import { Route, Switch } from 'react-router';
import { DashboardAdminComponent, DashboardStudentComponent } from './components';

export function DashboardPage() {
  return (
    <div>
      <Switch>
        <Route path="/admin/dashboard">
          <DashboardAdminComponent />
        </Route>

        <Route path="/admin/student">
          <DashboardStudentComponent />
        </Route>
      </Switch>
    </div>
  );
}