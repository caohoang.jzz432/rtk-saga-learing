import {
  Box,
  Button,
  CircularProgress,
  makeStyles,
  Paper,
  Theme,
  Typography,
} from '@material-ui/core';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { authActions } from '../auth.slice';

export interface LoginPageProps {}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh',
  },
  box: {
    padding: theme.spacing(4),
  },
}));

export function LoginPage(props: LoginPageProps) {
  const classes = useStyles();
  const dispatch = useAppDispatch();
  const isLogging = useAppSelector((state) => state.auth.logging);

  const onLoginClickHandler = () => {
    dispatch(
      authActions.login({
        username: 'admin',
        password: 'default',
      })
    );
  };

  return (
    <div className={classes.root}>
      <Paper elevation={1} className={classes.box}>
        <Typography variant="h5" component="h1">
          Student Management
        </Typography>

        <Box mt={4}>
          <Button fullWidth variant="contained" color="primary" onClick={onLoginClickHandler}>
            {isLogging && (
              <CircularProgress size={20} color="secondary" style={{ marginRight: 10 }} />
            )}
            Fake Login
          </Button>
        </Box>
      </Paper>
    </div>
  );
}
