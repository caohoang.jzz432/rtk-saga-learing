import { call, delay, fork, put, take } from '@redux-saga/core/effects';
import { PayloadAction } from '@reduxjs/toolkit';
import { replace } from 'connected-react-router';
import { authActions, LoginPayload } from './auth.slice';

function* handleLogin(payload: LoginPayload) {
  try {
    yield delay(1000);

    console.log('Handle Login', payload);

    localStorage.setItem('access_token', 'abcxyz');

    yield put(
      authActions.loginSuccess({
        id: 1,
        name: 'Easy Frontend',
      })
    );

    // redirect to admin page
    yield put(replace('/admin/dashboard'));
  } catch (error: any) {
    yield put(authActions.loginFailed(error.message));
  }
}

function* handleLogout() {
  yield delay(1000);

  console.log('Handle Logout');

  localStorage.removeItem('access_token');

  // redirect to login page
  yield put(replace('/login'));
}

/**
 * Watcher
 */
function* watchLoginFlow() {
  while (true) {
    const isLoggedIn = Boolean(localStorage.getItem('access_token'));

    if (!isLoggedIn) {
      const action: PayloadAction<LoginPayload> = yield take(authActions.login.toString());

      yield fork(handleLogin, action.payload);
    }

    // use action.toString() if you don't have to care about type error
    yield take(authActions.logout.toString());
    // or use yield take(authActions.logout.type);

    yield call(handleLogout); // blocking call, diff from fork (non-blocking call)
  }
}

export function* authSaga() {
  yield fork(watchLoginFlow);
}
