import { Box, makeStyles } from '@material-ui/core';
import { HeaderComponent, SidebarComponent } from 'components/common';
import { DashboardPage } from 'features/dashboard/pages';

export interface AdminLayoutProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateRows: 'auto 1fr',
    gridTemplateColumns: '250px 1fr',
    gridTemplateAreas: `"header header" 
                        "sidebar main"`,
    minHeight: '100vh',
  },

  header: {
    gridArea: 'header',
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  sidebar: {
    gridArea: 'sidebar',
    borderRight: `1px solid ${theme.palette.divider}`,
    backgroundColor: theme.palette.background.paper,
  },
  main: {
    gridArea: 'main',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 3),
  },
}));

export function AdminLayout(props: AdminLayoutProps) {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <HeaderComponent />
      </Box>
      <Box className={classes.sidebar}>
        <SidebarComponent />
      </Box>
      <Box className={classes.main}>
        <DashboardPage />
      </Box>
    </Box>
  );
}
