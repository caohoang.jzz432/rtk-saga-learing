import { Redirect, Route, RouteProps } from 'react-router';

export function PrivateRouteComponent(props: RouteProps) {
  const isLoggedIn = Boolean(localStorage.getItem('access_token'));

  if (!isLoggedIn) {
    return <Redirect to={'/login'} />;
  }

  return <Route {...props} />;
}
