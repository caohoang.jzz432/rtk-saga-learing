import cityApi from 'api/city.api';
import { NotFoundComponet, PrivateRouteComponent } from 'components/common';
import { AdminLayout } from 'components/layout';
import { LoginPage } from 'features/auth/pages';
import { useEffect } from 'react';
import { Route, Switch } from 'react-router';

function App() {
  useEffect(() => {
    cityApi.getAll().then((response) => console.log(response));
  }, []);

  return (
    <div>
      <Switch>
        <Route path="/login">
          <LoginPage />
        </Route>

        <PrivateRouteComponent path="/admin">
          <AdminLayout />
        </PrivateRouteComponent>

        <PrivateRouteComponent>
          <NotFoundComponet />
        </PrivateRouteComponent>
      </Switch>
    </div>
  );
}

export default App;
