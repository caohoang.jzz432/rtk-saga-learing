import { authSaga } from 'features/auth/auth.saga';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  console.log('Root Saga');

  yield all([authSaga()]);
}
